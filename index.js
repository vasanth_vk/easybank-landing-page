const hamburger = document.querySelector(".hamburger");
const navbar = document.querySelector(".navbar");
const overlay = document.querySelector(".overlay");
const body = document.querySelector("body");

hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("hamburger-close");
    navbar.classList.toggle("navbar-open");
    body.classList.toggle("no-scroll");
    overlay.classList.toggle("overlay-active");
});
